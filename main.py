import json
from io import BytesIO

import torchvision.transforms as transforms
from PIL import Image
from flask import Flask, request  # importing the module
from numpy import asarray

from facial_emotions import HSEmotionRecognizer

model_name = 'enet_b0_8_best_afew'
fer = HSEmotionRecognizer(model_name=model_name, device='cpu')
# model = tf.keras.models.load_model('mask_detector2.model')
app = Flask(__name__)  # instantiating flask object

import torch
from torch import nn
from torchvision.transforms import Resize


class MaskDetector(nn.Module):
    def __init__(self, loss_function):
        super(MaskDetector, self).__init__()

        self.loss_function = loss_function

        self.conv2d_1 = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=(3, 3), padding=(1, 1)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2))
        )

        self.conv2d_2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=(3, 3), padding=(1, 1)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2))
        )

        self.conv2d_3 = nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=(3, 3), padding=(1, 1), stride=(3, 3)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2))
        )

        self.linearLayers = nn.Sequential(
            nn.Linear(in_features=2048, out_features=1024),
            nn.ReLU(),
            nn.Linear(in_features=1024, out_features=2)
        )

        for sequential in [self.conv2d_1, self.conv2d_2, self.conv2d_3, self.linearLayers]:
            for layer in sequential.children():
                if isinstance(layer, (nn.Linear, nn.Conv2d)):
                    nn.init.xavier_uniform_(layer.weight)

    def forward(self, x):
        out = self.conv2d_1(x)
        out = self.conv2d_2(out)
        out = self.conv2d_3(out)
        out = out.view(-1, 2048)
        out = self.linearLayers(out)

        return out

    def add_optimizer(self, optimizer):
        self.optimizer = optimizer


loss_function = nn.CrossEntropyLoss(weight=torch.tensor([0.5, 0.5]))

# initializing the model
model_mask = MaskDetector(loss_function)
model_mask.load_state_dict(torch.load('final_model_mask.pt', torch.device('cpu')))
model_mask.eval()


# image = Image.open('upload.jpg').convert("RGB").resize((100, 100), Image.ANTIALIAS)


@app.route('/faces_api', methods=['POST', 'GET'])  # defining a route in the application
def func():  # writing a function to be executed
    input_image = Image.open(BytesIO(request.data))
    a = input_image.convert("RGB").resize((224, 224), Image.ANTIALIAS)
    emotion, scores = fer.predict_emotions(asarray(a), logits=True)
    # Define a transform to convert PIL
    # image to a Torch tensor
    transform = transforms.Compose([
        transforms.PILToTensor()
    ])
    img_tensor = transform(a)
    re_img = Resize((100, 100))(img_tensor).reshape((1, 3, 100, 100)).float()
    prediction2 = model_mask(re_img)
    print("mask: ", prediction2)
    print("\n")
    print("emotion: ", emotion, scores)
    reply = list(asarray(prediction2[0].detach().numpy(), dtype='float'))
    return json.dumps({"emotion": str(emotion), "scores": list(scores.astype(float)),
                       "mask": reply})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=False)
