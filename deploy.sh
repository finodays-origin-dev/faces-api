export container_name=${1//\//.}
export params=$2
echo $container_name

export container_id=$(docker ps -qf "name=$container_name")
if [ -n "$container_id" ]; then 
    docker stop $container_id;
    docker rm $container_name; 
fi

export container_id=$(docker ps -aqf "name=$container_name")
if [ -n "$container_id" ]; then 
    docker rm $container_name; 
fi

docker image rm $1

export tag=$3
export docker_run_command="docker run "$params" --name $container_name $1:$tag"
eval $docker_run_command

